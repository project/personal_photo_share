; Personal Photo Share make file
core = "7.x"
api = "2"
; comment this out in to use on drupal.org
; projects[drupal][version] = "7.16"

; Modules
projects[admin_menu][version] = "3.0-rc3"
projects[admin_menu][subdir] = "contrib"

projects[ctools][version] = "1.2"
projects[ctools][subdir] = "contrib"

projects[context][version] = "3.0-beta4"
projects[context][subdir] = "contrib"

projects[context_condition_theme][version] = "1.0"
projects[context_condition_theme][subdir] = "contrib"

projects[date][version] = "2.6"
projects[date][subdir] = "contrib"

projects[profiler_builder][version] = "1.x-dev"
projects[profiler_builder][subdir] = "contrib"

projects[features][version] = "1.0"
projects[features][subdir] = "contrib"

projects[ftools][version] = "1.6"
projects[ftools][subdir] = "contrib"

projects[feeds][version] = "2.0-alpha7"
projects[feeds][subdir] = "contrib"

projects[filefield_paths][version] = "1.0-beta3"
projects[filefield_paths][subdir] = "contrib"

projects[link][version] = "1.0"
projects[link][subdir] = "contrib"

projects[flexslider][version] = "1.x-dev"
projects[flexslider][subdir] = "contrib"

projects[video_filter][version] = "3.0"
projects[video_filter][subdir] = "contrib"

projects[mailhandler][version] = "2.x-dev"
projects[mailhandler][subdir] = "contrib"

projects[imce][version] = "1.5"
projects[imce][subdir] = "contrib"

projects[imce_crop][version] = "1.0"
projects[imce_crop][subdir] = "contrib"

projects[media][version] = "1.2"
projects[media][subdir] = "contrib"

projects[nodeformcols][version] = "1.x-dev"
projects[nodeformcols][subdir] = "contrib"

projects[better_formats][version] = "1.0-beta1"
projects[better_formats][subdir] = "contrib"

projects[boxes][version] = "1.0"
projects[boxes][subdir] = "contrib"

projects[diff][version] = "2.0"
projects[diff][subdir] = "contrib"

projects[entity][version] = "1.0-rc3"
projects[entity][subdir] = "contrib"

projects[job_scheduler][version] = "2.0-alpha3"
projects[job_scheduler][subdir] = "contrib"

projects[libraries][version] = "2.0"
projects[libraries][subdir] = "contrib"

projects[mail_edit][version] = "1.0"
projects[mail_edit][subdir] = "contrib"

projects[module_filter][version] = "1.7"
projects[module_filter][subdir] = "contrib"

projects[pathauto][version] = "1.2"
projects[pathauto][subdir] = "contrib"

projects[strongarm][version] = "2.0"
projects[strongarm][subdir] = "contrib"

projects[token][version] = "1.4"
projects[token][subdir] = "contrib"

projects[transliteration][version] = "3.1"
projects[transliteration][subdir] = "contrib"

projects[subscriptions][version] = "1.1"
projects[subscriptions][subdir] = "contrib"

projects[textbook][version] = "1.0-rc1"
projects[textbook][subdir] = "contrib"

projects[delta][version] = "3.0-beta11"
projects[delta][subdir] = "contrib"

projects[tinynav][version] = "1.1"
projects[tinynav][subdir] = "contrib"

projects[ckeditor_link][version] = "2.2"
projects[ckeditor_link][subdir] = "contrib"

projects[imce_wysiwyg][version] = "1.0"
projects[imce_wysiwyg][subdir] = "contrib"

projects[lightbox2][version] = "1.0-beta1"
projects[lightbox2][subdir] = "contrib"

projects[wysiwyg][version] = "2.2"
projects[wysiwyg][subdir] = "contrib"

projects[wysiwyg_template][version] = "2.9"
projects[wysiwyg_template][subdir] = "contrib"

projects[views][version] = "3.5"
projects[views][subdir] = "contrib"

projects[views_bulk_operations][version] = "3.0"
projects[views_bulk_operations][subdir] = "contrib"

projects[views_fluid_grid][version] = "3.0"
projects[views_fluid_grid][subdir] = "contrib"

; Themes
; responsive
projects[responsive][type] = "theme"
projects[responsive][version] = "1.6"
projects[responsive][subdir] = "contrib"
; chamfer
projects[chamfer][type] = "theme"
projects[chamfer][version] = "1.x-dev"
projects[chamfer][subdir] = "contrib"
; rubik
projects[rubik][type] = "theme"
projects[rubik][version] = "4.0-beta8"
projects[rubik][subdir] = "contrib"
; omega
projects[omega][type] = "theme"
projects[omega][version] = "3.1"
projects[omega][subdir] = "contrib"
; tao
projects[tao][type] = "theme"
projects[tao][version] = "3.0-beta4"
projects[tao][subdir] = "contrib"

; Libraries
libraries[profiler][directory_name] = "profiler"
libraries[profiler][type] = "library"
libraries[profiler][destination] = "libraries"
libraries[profiler][download][type] = "get"
libraries[profiler][download][url] = "http://ftp.drupal.org/files/projects/profiler-7.x-2.x-dev.tar.gz"

libraries[ckeditor][directory_name] = "ckeditor"
libraries[ckeditor][type] = "library"
libraries[ckeditor][destination] = "libraries"
libraries[ckeditor][download][type] = "get"
libraries[ckeditor][download][url] = "http://download.cksource.com/CKEditor/CKEditor/CKEditor%203.6.4/ckeditor_3.6.4.tar.gz"

libraries[flexslider][directory_name] = "flexslider"
libraries[flexslider][type] = "library"
libraries[flexslider][destination] = "libraries"
libraries[flexslider][download][type] = "get"
libraries[flexslider][download][url] = "https://github.com/downloads/woothemes/FlexSlider/FlexSlider-1.8.zip"

