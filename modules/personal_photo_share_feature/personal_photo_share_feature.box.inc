<?php
/**
 * @file
 * personal_photo_share_feature.box.inc
 */

/**
 * Implements hook_default_box().
 */
function personal_photo_share_feature_default_box() {
  $export = array();

  $box = new stdClass();
  $box->disabled = FALSE; /* Edit this to true to make a default box disabled initially */
  $box->api_version = 1;
  $box->delta = 'personal_photo_share';
  $box->plugin_key = 'simple';
  $box->title = '<none>';
  $box->description = 'Footer message';
  $box->options = array(
    'body' => array(
      'value' => '<p>Copyright 2012</p>
',
      'format' => 'textbook_editor',
    ),
  );
  $export['personal_photo_share'] = $box;

  return $export;
}
